import 'package:animated_theme_switcher/animated_theme_switcher.dart';
import 'package:attendance/core/authentication_manager.dart';
import 'package:attendance/models/dto/profile.dart';
import 'package:attendance/views/homepage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:badges/badges.dart' as badges;
import 'package:get/get.dart';
import 'package:motion_tab_bar_v2/motion-tab-bar.dart';
import 'package:motion_tab_bar_v2/motion-tab-controller.dart';



class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver, AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  final RxInt _currentIndex = 0.obs;
  // final ShoppingCartViewModel shoppingCart = Get.find<ShoppingCartViewModel>();
  late final PageController _pageController;

  late final List<CachedNetworkImageProvider> multiImageProvider;
  // TabController _tabController;
  MotionTabBarController? _motionTabBarController;
  AuthenticationManager authManager = Get.find();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    final int initialTabIndex = Get.arguments ?? 0;
    _currentIndex.value = initialTabIndex;
    _pageController = PageController(initialPage: initialTabIndex);

    // _changeTab(initialTabIndex);
    // shoppingCart.getCartItemList();
    _motionTabBarController = MotionTabBarController(length: 4, vsync: this, initialIndex: initialTabIndex);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return ThemeSwitchingArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Home'),
          actions: [
            IconButton(onPressed: (){
              authManager.logOut();
            }, icon: const Icon(Icons.logout_rounded))
          ],
        ),
        body: PageView(
          controller: _pageController,
          onPageChanged: (index) {
            _currentIndex.value = index;
            // print("test motion : $index");
            _motionTabBarController!.index = index;
          },
          children: const [

             HomePage(),
          ],
        ),
        bottomNavigationBar: _buildBottomNavigationBar(),
      ),
    );
  }

  @override
  void dispose() {
    _pageController.dispose();
    WidgetsBinding.instance.removeObserver(this);
    // shoppingCart.dispose();
    super.dispose();
    _motionTabBarController!.dispose();
  }

  Widget _buildBottomNavigationBar() {
    return Obx(() {
     print("=======> ${_currentIndex.value}");
      return MotionTabBar(
        controller: _motionTabBarController, // Add this controller if you need to change your tab programmatically
        initialSelectedTab: "Home",
        useSafeArea: true, // default: true, apply safe area wrapper
        labels: const ["Home", "Profile", "Settings"],
        icons: const [Icons.home, Icons.people_alt, Icons.settings],
        tabSize: 50,
        tabBarHeight: 55,
        textStyle: const TextStyle(
          fontSize: 12,
          color: Colors.black,
          fontWeight: FontWeight.w500,
        ),
        tabIconColor: Colors.blue[600],
        tabIconSize: 28.0,
        tabIconSelectedSize: 26.0,
        tabSelectedColor: Colors.blue[900],
        tabIconSelectedColor: Colors.white,
        tabBarColor: Colors.white,
        onTabItemSelected: (int value) {
          setState(() {
            _pageController.jumpToPage(value);
            _motionTabBarController!.index = value;
          });
        },
      );
    });
  }
}
