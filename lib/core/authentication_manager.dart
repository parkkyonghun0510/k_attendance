
import 'package:attendance/core/cache_manager.dart';
import 'package:get/get.dart';

class AuthenticationManager extends GetxController with CacheManager {
      final isLogged = false.obs;

      void logOut(){
        isLogged.value = false;
        removeToken();
        print("token ====> Logout");
      }

      void login(String? token) async{
        isLogged.value = true;
        saveToken(token);
        print("token ====> : $token");
      }

      void checkLoginStatus(){
        final token = getToken();
        print("checkLoginStatus ====> : $token");
        print("isLogged.value ====> : ${isLogged.value}");
        if(token != null){
          isLogged.value = true;
        }
      }
}